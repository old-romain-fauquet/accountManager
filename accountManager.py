# -*- encoding : utf-8 -*-

class Compte():
	def __init__(self):
		self.plateformes={} # Dictionnaire de Plateforme()

	def ajouter_compte(self, plateforme, login, password, mail):
		self.ajouter_plateforme(plateforme)
		self.plateformes[plateforme].add_login(login, password, mail)

	def ajouter_plateforme(self, plateforme):
		if plateforme in self.plateformes:
			return 0
		self.plateformes[plateforme] = Plateforme(plateforme)

	def retirer_plateforme(self, plateforme):
		del self.plateformes[plateforme]

	def sauvegarder_compte(self, fichier="account.cpt"):
		if fichier == "":
			fichier = "account.cpt"
		file = open(fichier, "w")
		for plat in self.plateformes:
			for login in self.plateformes[plat].login:
				file.write(plat + ";" + login + ";" + self.plateformes[plat].login[login].password + ";" + self.plateformes[plat].login[login].mail + "\n")
		file.close()

	def charger_compte(self, fichier="account.cpt"):
		self.plateformes = {}
		file = open(fichier, "r")
		content = file.read()
		file.close()
		comptes = content.split("\n")
		for acc in comptes:
			if acc != "":
				cpt = acc.split(";")
				if cpt[0] in self.plateformes:
					self.plateformes[cpt[0]].login[cpt[1]] = Login(cpt[1])
				else:
					self.plateformes[cpt[0]] = Plateforme(cpt[0])
					self.plateformes[cpt[0]].login[cpt[1]] = Login(cpt[1])
				self.plateformes[cpt[0]].login[cpt[1]].password = cpt[2]
				self.plateformes[cpt[0]].login[cpt[1]].mail = cpt[3]

	def afficher_compte(self, fplateforme="", flogin=""):
		print("Plateforme | Login | Mot de passe | Mail")
		print("========================================")
		for plat in self.plateformes:
			for login in self.plateformes[plat].login:
				if (plat == fplateforme or fplateforme =="") and (login == flogin or flogin == ""):
					print(plat + " | " + login + " | " + self.plateformes[plat].login[login].password + " | " + self.plateformes[plat].login[login].mail)
		print()
		input("Appuyez sur [ENTER] pour continuer")

class Plateforme():
	def __init__(self, nom):
		self.nom = nom
		self.login = {} # Dictionnaire de Login()

	def add_login(self, login, password="", mail=""):
		self.login[login] = Login(login)
		self.login[login].password = password
		self.login[login].mail = mail

	def edit_login(self, login, password="", mail=""):
		try:
			if password != "":
				self.login[login].set_password(password)
			if mail != "":
				self.login[login].set_mail(mail)
		except:
			print("Erreur ! Le login : " + login + " n'extiste pas dans la plateforme : " + self.nom + " !")
	
	def retirer_login(self, login, password, mail):
		try:
			if self.login[login].password == password and self.login[login].mail == mail:
				del self.login[login]
		except:
			print("Erreur ! Les donnees que vous avez saisie ne corresepondent pas !")

class Login():
	def __init__(self, login):
		self.login = login
		self.password = ""
		self.mail = ""

compte = Compte()

def menu():
	global compte
	filtre_plateforme = ""
	filtre_login = ""
	ok=1
	while ok==1:
		try:
			print("===============accountManager===============")
			print("\t1. Afficher compte")
			print("\t2. Filtre d'affichage")
			print("\t3. Ajouter un compte")
			print("\t4. Sauvegarder")
			print("\t5. Charger")
			print("\t0. Quitter")
			print("\t\t\tMade by Sirfanas.")
			print("===========================================")
			
			choix = input()
			if choix == "1":
				compte.afficher_compte(filtre_plateforme, filtre_login)
			if choix == "2":
				filtre_plateforme = input("Saisissez le filtre de la plateforme souhaitez.")
				filtre_login = input("Saisissez le filtre du login souhaitez.")
			if choix == "3":
				p, l, mdp, m = saisie_compte()
				compte.ajouter_compte(p, l, mdp, m)
			if choix == "5":
				fichier = input("Saisissez le nom du fichier a charger ([ENTER] pour le fichier par defaut, -1 pour annuler")
				if fichier != "-1":
					compte.charger_compte()
			if choix == "4":
				compte.sauvegarder_compte(input("Saisissez le nom du fichier dans lequel enregistrer les donnees."))
			if choix == "0":
				ok=0
		except:
			print("Une erreur a eu lieu !")

def saisie_compte():
	p = input("Veuillez saisir la plateforme du compte : ")
	l = input("Veuillez saisir le login du compte : ")
	mdp = input("Veuillez saisir le mot de passe du compte : ")
	m = input("Veuillez saisir l'adresse mail du compte : ")
	return p, l, mdp, m

menu()